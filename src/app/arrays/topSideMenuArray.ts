export const items = [
  {
    name: "John Wick",
    action: "commented",
    preview: "Well guys, this is just a joke tho...",
    timestamp: "1 day ago",
    pic: "https://images.pexels.com/photos/4152567/pexels-photo-4152567.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    name: "Mark Zuck",
    action: "tagged you on a video",
    preview: "A beautiful day in Rusia...",
    timestamp: "50 minutes ago",
    pic: "https://images.pexels.com/photos/4151057/pexels-photo-4151057.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
  },
  {
    name: "Angela Tan",
    action: "posted a document",
    preview: "I just created a document about...",
    timestamp: "3 hours ago",
    pic: "https://images.pexels.com/photos/4680107/pexels-photo-4680107.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    name: "Jody Mantap",
    action: "liked your video",
    preview: "Room tour in my new apartment...",
    timestamp: "Just now",
    pic: "https://images.pexels.com/photos/5956910/pexels-photo-5956910.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
];
