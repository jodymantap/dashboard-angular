export const items = [
  {
    text: "Google",
    pic: "https://images.pexels.com/photos/1482061/pexels-photo-1482061.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Dribbble",
    pic: "https://images.pexels.com/photos/5837017/pexels-photo-5837017.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Microsoft",
    pic: "https://images.pexels.com/photos/4491606/pexels-photo-4491606.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Behance",
    pic: "https://images.pexels.com/photos/6444/pencil-typography-black-design.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Weather Channel",
    pic: "https://images.pexels.com/photos/459451/pexels-photo-459451.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Zenius",
    pic: "https://images.pexels.com/photos/4866043/pexels-photo-4866043.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Yahoo",
    pic: "https://images.pexels.com/photos/211290/pexels-photo-211290.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Whatsapp",
    pic: "https://images.pexels.com/photos/4497814/pexels-photo-4497814.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260",
  },
  {
    text: "Gojek",
    pic: "https://images.pexels.com/photos/5835017/pexels-photo-5835017.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
  {
    text: "Tokopedia",
    pic: "https://images.pexels.com/photos/919436/pexels-photo-919436.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
  },
];
