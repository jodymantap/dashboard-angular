export const items = [
  {
    menu_name: "Videos",
    menu_action: "Browse all videos",
    menu_main_tile: {
      tile_first_text: "How to improve your skills",
      tile_second_text: "Waseem Arshad",
    },
    menu_side_tiles: [
      { tile_first_text: "John Wick", tile_second_text: "1412 views" },
      { tile_first_text: "Mark Zuck", tile_second_text: "9812 views" },
      { tile_first_text: "Ahmed Yen", tile_second_text: "5023 views" },
      { tile_first_text: "Anna Jane", tile_second_text: "8123 views" },
      { tile_first_text: "Upload", tile_second_text: "Your Own Video" },
    ],
    menu_pics: [
      "https://images.pexels.com/photos/1615824/pexels-photo-1615824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/4911192/pexels-photo-4911192.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347919/pexels-photo-6347919.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347613/pexels-photo-6347613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347607/pexels-photo-6347607.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    ],
  },
  {
    menu_name: "People",
    menu_action: "View all",
    menu_main_tile: {
      tile_first_text: "Waseem Arshad",
      tile_second_text: "User Inteface Designer",
    },
    menu_side_tiles: [
      { tile_first_text: "John Wick", tile_second_text: "" },
      { tile_first_text: "Mark Zuck", tile_second_text: "" },
      { tile_first_text: "Ahmed Yen", tile_second_text: "" },
      { tile_first_text: "Anna Jane", tile_second_text: "" },
      { tile_first_text: "Share", tile_second_text: "Your Work" },
    ],
    menu_pics: [
      "https://images.pexels.com/photos/1615824/pexels-photo-1615824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/4911192/pexels-photo-4911192.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347919/pexels-photo-6347919.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347613/pexels-photo-6347613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347607/pexels-photo-6347607.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    ],
  },
  {
    menu_name: "Documents",
    menu_action: "Browse all documents",
    menu_main_tile: {
      tile_first_text: "Mobile UI & UX Guide 2013",
      tile_second_text: "By Williams Makhoja",
    },
    menu_side_tiles: [
      { tile_first_text: "John Wick", tile_second_text: "" },
      { tile_first_text: "Mark Zuck", tile_second_text: "" },
      { tile_first_text: "Ahmed Yen", tile_second_text: "" },
      { tile_first_text: "Anna Jane", tile_second_text: "" },
      { tile_first_text: "Share", tile_second_text: "Your Document" },
    ],
    menu_pics: [
      "https://images.pexels.com/photos/1615824/pexels-photo-1615824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/4911192/pexels-photo-4911192.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347919/pexels-photo-6347919.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347613/pexels-photo-6347613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
      "https://images.pexels.com/photos/6347607/pexels-photo-6347607.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    ],
  },
];
